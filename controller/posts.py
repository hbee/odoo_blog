# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.http import Controller, route, request
from openerp.addons.website.models.website import slug
from werkzeug.utils import redirect
from wtforms import Form, StringField, TextAreaField, validators


class Posts(Controller):

    @route('/posts/', website=True)
    def list(self):
        posts = request.env['blog.post']
        posts = posts.search([])
        return request.render('odoo_blog.posts_list',
                              {'posts': posts})

    @route('/posts/new/', website=True)
    def new(self, **form_data):
        form = PostsNewForm(request.httprequest.form)
        if request.httprequest.method == 'POST' and form.validate():
            posts = request.env['blog.post']
            posts.create({
                'title': form_data.get('title', ''),
                'content': form_data.get('content', ''),
            })
            return redirect("/posts/")
        return request.render('odoo_blog.posts_new', {'form': form})

    @route('/posts/<model("blog.post"):post>/view/', website=True)
    def view(self, post):
        return request.render('odoo_blog.posts_view',
                              {'post': post})

    @route('/posts/<model("blog.post"):post>/delete/', website=True)
    def delete(self, post):
        post.unlink()
        return redirect("/posts/")

    @route('/posts/<model("blog.post"):post>/edit/', website=True)
    def edit(self, post, **form_data):
        form = PostsNewForm(request.httprequest.form)
        if request.httprequest.method == 'POST' and form.validate():
            post.write({
                'title': form_data.get('title', ''),
                'content': form_data.get('content', ''),
            })
            return redirect("/posts/%s/view" % slug(post))
        form.title.data = post.title
        form.content.data = post.content
        return request.render('odoo_blog.posts_edit',
                              {'form': form, 'post': post})


class PostsNewForm(Form):
    title = StringField('Title', [validators.Required()])
    content = TextAreaField('Content', [validators.Required()])
