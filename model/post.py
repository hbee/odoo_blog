# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.models import BaseModel
from openerp import fields


class Post(BaseModel):

    _name = 'blog.post'

    title = fields.Char('Name')
    content = fields.Text('Content')
